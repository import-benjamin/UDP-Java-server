package code;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UDPClient {
	public static void main(String arg[]){
		// declaration des references d objets
		InetAddress adr;
		int port=5000;
		DatagramPacket packet;
		byte[] inputData;
		byte[] outputData;
		try{
			DatagramSocket socket = new DatagramSocket();
			// adrcontient l'@IP de la partie serveur
			adr = InetAddress.getByName("localhost");
			// donnees à envoyer : chaine à convertir en octets
			String envoi = "CIT:3";
			outputData = envoi.getBytes();
			/* creation paquet données en précisant @IP/port sur
			lequel le serveur écoute : 5000*/
			packet = new DatagramPacket(outputData, outputData.length, adr, port);

			// creation socket, sans la lier à un port particulier
			// envoi du paquet via la socket
			socket.send(packet);
			System.out.println("Envoi au serveur "+packet.getSocketAddress() +" du message : "+envoi);
			inputData = new byte[2000];
			// tableau de 50 octets qui contiendra les données reçues
			packet = new DatagramPacket(inputData, inputData.length);
			// attente paquet envoye sur la socket du client
			socket.receive(packet);
			// récupération/affichage des données(chaîne caractères)
			String chaine = new String(packet.getData(), 0, packet.getLength());
			System.out.println(" recu du serveur : "+chaine);
			socket.close();
		}
		catch (Exception e){
			System.out.println("erreur : " + e.getMessage());
		}
	}
}
